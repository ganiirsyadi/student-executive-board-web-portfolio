<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>BISMIT Projects</title>
    <link rel="icon" type="image/ico" href="{{asset('images/project/logoBisMitSmall.png')}}" />
    <!-- CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('css/contact.css')}}">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Muli&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Rubik:700&display=swap" rel="stylesheet">
</head>
<body>
  <section class="contact" style="background-image: linear-gradient(rgba(209, 209, 209, 0.7),rgba(209, 209, 209, 0.7)),url({{asset('images/project/parallax.jpg')}});">
    <div class="header"><h1 id="contactTitle" class="mainTitle">INTERESTED?</h1></div>
    <div class="contactElements container">
      <div class="row justify-content-center">
        <div class="col-10 col-md-5 contactElement flex-column align-items-center d-flex justify-content-center">
          <a class="contactBtn" href="https://wa.me/6287788080742">
            <img class="img-fluid" src="{{asset('images/project/phone-call.png')}}" alt="">
          </a>
          <a href="https://wa.me/6287788080742" class="contactDesc">Nata (+62) 812345678</a>
        </div>
        <div class="col-10 col-md-5 contactElement flex-column align-items-center d-flex justify-content-center order-2">
          <a target="_blank" href="https://bem.cs.ui.ac.id/bismit-final/" class="contactBtn">
              <img class="img-fluid" src="{{asset('images/project/search.png')}}" alt="">
          </a>
          <a href="https://bem.cs.ui.ac.id/bismit-final/" class="contactDesc">Hire through website</a>
        </div>
        </div>
      </div>
    </div>
  </section>
</body>
<!-- scripts -->
<!-- <script src="{{asset('js/project.js')}}"></script> -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</html>